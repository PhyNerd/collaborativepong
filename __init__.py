from st3m.application import Application, ApplicationContext
from st3m.input import InputController, InputState
from st3m.ui.interactions import CapScrollController
from ctx import Context
import st3m.run
import math 
import random 
import leds
import json

CONFIG_PATH = "/flash/pong.json"
PADDLE_WIDTH = math.pi/8
PADDLE_SPEED = 1/100
COLLISION_ACCELERATION = 1.1
START_BALL_SPEED = 1/50

class Pong(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)

        self.input = InputController()

        self.scrollOne = CapScrollController()
        self.scrollTwo = CapScrollController()

        self.startGame()

        self.playerOneAngle = 0
        self.playerTwoAngle = math.pi

        try:
            with open(CONFIG_PATH) as f:
                jsonData = f.read()
            data = json.loads(jsonData)
        except OSError:
            data = {}
        if "highScore" in data and type(data["highScore"]) == int:
            self.highScore = data["highScore"]
        else:
            self.highScore = 0

    def startGame(self):
        self.xPos = 0
        self.yPos = 0
        speed = START_BALL_SPEED
        angle = random.random() * 2 * math.pi
        self.xSpeed = speed * math.cos(angle)
        self.ySpeed = speed * math.sin(angle)

        self.score = 0
        leds.set_all_rgb(0,0,0)
        leds.update()
    
    def endGame(self):
        if(self.highScore < self.score):
            jsonData = json.dumps({"highScore": self.score})
            with open(CONFIG_PATH, "w") as outfile:
                outfile.write(jsonData)
            self.highScore = self.score
        self.startGame()


    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0,0,0).rectangle(-120, -120, 240, 240).fill()

        # Player areas
        ctx.line_width = 4
        ctx.rgb(0, 0, 255).arc(0, 0, 118, -3/10 * math.pi , 1/10 * math.pi, 0).stroke()
        ctx.rgb(0, 255, 0).arc(0, 0, 118, 9/10 * math.pi , 13/10 * math.pi, 0).stroke()

        # Player paddles
        ctx.line_width = 4
        ctx.rgb(0, 255, 0).arc(0, 0, 110, self.playerTwoAngle-PADDLE_WIDTH, self.playerTwoAngle+PADDLE_WIDTH, 0).stroke()
        ctx.rgb(0, 0, 255).arc(0, 0, 110, self.playerOneAngle-PADDLE_WIDTH, self.playerOneAngle+PADDLE_WIDTH, 0).stroke()

        # HighScore
        ctx.text_align = ctx.CENTER
        ctx.font_size = 20
        ctx.rgb(255,255,255).move_to(0,-90).text("HS:"+str(self.highScore))

        # Score
        ctx.text_align = ctx.CENTER
        ctx.font_size = 30
        ctx.rgb(255,255,255).move_to(0,10).text(str(self.score))

        # Ball
        ctx.rgb(255, 0, 0).rectangle(self.xPos-3, self.yPos-3, 6, 6).fill()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self.processPlayerInput(delta_ms)
        self.calcBallMovement(delta_ms)

    def processPlayerInput(self, delta_ms: int):
        self.scrollOne.update(self.input.captouch.petals[2].gesture, delta_ms)
        self.playerOneAngle = self.scrollOne.position[1] * PADDLE_SPEED

        self.scrollTwo.update(self.input.captouch.petals[8].gesture, delta_ms)
        self.playerTwoAngle = math.pi + ( self.scrollTwo.position[1] * PADDLE_SPEED )

        self.playerOneAngle = self.playerOneAngle % (2*math.pi)
        self.playerTwoAngle = self.playerTwoAngle % (2*math.pi)

    def calcBallMovement(self, delta_ms: int):
        distanceFromCenter = math.sqrt( abs(self.xPos)**2 + abs(self.yPos)**2 )
        if distanceFromCenter >= 105:
            ballAngle = math.atan2(self.yPos, self.xPos) % (math.pi*2) 
            
            # ensure ball never goes out of bound
            self.xPos = 105 * math.cos(ballAngle)
            self.yPos = 105 * math.sin(ballAngle)
          
            if self.processCollision(self.playerOneAngle, ballAngle):
                leds.set_all_rgb(0,0,255)
                leds.update()
            elif self.processCollision(self.playerTwoAngle, ballAngle):
                leds.set_all_rgb(0,255,0)
                leds.update()
            else:
                self.endGame()

        self.xPos += delta_ms * self.xSpeed
        self.yPos += delta_ms * self.ySpeed

    def processCollision(self, peadleAngle: float, ballAngle: float):
        if self.wasPeadleTouched(peadleAngle, ballAngle):
            ballSpeedAngle = math.atan2(self.ySpeed, self.xSpeed) % (math.pi*2)
            ballSpeed = math.sqrt(self.ySpeed**2+self.xSpeed**2)

            newBallSpeedAngle = ( ballSpeedAngle - 2 * (ballSpeedAngle - peadleAngle) + math.pi)%(2*math.pi)
            newBallSpeed = COLLISION_ACCELERATION * ballSpeed
            self.ySpeed = newBallSpeed * math.sin(newBallSpeedAngle)
            self.xSpeed = newBallSpeed * math.cos(newBallSpeedAngle)
            self.score += 1
            
            return True
        return False

    def wasPeadleTouched(self, peadleAngle: float, ballAngle: float):
        return min( abs(ballAngle - peadleAngle), 2 * math.pi - abs(ballAngle - peadleAngle) ) <= PADDLE_WIDTH

if __name__ == '__main__':
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(Pong(ApplicationContext()))